<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

use Interactiv4\Contracts\SPL\Config\Api\Exception\CouldNotCompareConfigException;
use LogicException;
use RuntimeException;

/**
 * Interface ConfigCompareInterface.
 *
 * Compare expected config against current one.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface ConfigCompareInterface
{
    /**
     * Compare expected config against current one.
     * It MAY use optionally supplied context to determine how / where config should be retrieved / compared.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param mixed $value   Mandatory, it can be of any type, recommended: int|string|bool
     * @param array $context Optional, additional data to determine how / where config should be retrieved / compared.
     * @param bool  $strict  Whether comparison between current config and supplied one should be strict or not.
     *
     * @return bool Returning a boolean without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting config.
     *
     * @throws CouldNotCompareConfigException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function isConfig(
        $value,
        array $context = [],
        bool $strict = false
    ): bool;
}
