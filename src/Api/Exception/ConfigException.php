<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api\Exception;

use Exception;

/**
 * Class ConfigException.
 *
 * All exceptions raised by config interfaces MUST be either:
 * - Subclasses of this class
 * - @see \RuntimeException or its subclasses
 *
 * For @see \LogicException or more specific exceptions, wrap them as previous into appropriate exception type:
 * @see CouldNotCompareConfigException
 * @see CouldNotPutConfigException
 * @see CouldNotReadConfigException
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 */
abstract class ConfigException extends Exception
{
}
