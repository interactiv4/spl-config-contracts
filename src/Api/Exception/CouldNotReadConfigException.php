<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api\Exception;

/**
 * Class CouldNotReadConfigException.
 *
 * Thrown when a non-runtime error occurs while reading config.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 */
final class CouldNotReadConfigException extends ConfigException
{
}
