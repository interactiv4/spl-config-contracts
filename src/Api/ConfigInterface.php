<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

/**
 * Interface ConfigInterface.
 *
 * Read / put / compare current config.
 *
 * This is a convenience interface intended for implementors that provide read / put / compare config capabilities.
 *
 * @see ConfigReadInterface
 * @see ConfigCompareInterface
 * @see ConfigPutInterface
 * @see ConfigReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface ConfigInterface extends
    ConfigReadInterface,
    ConfigCompareInterface,
    ConfigPutInterface
{
}
