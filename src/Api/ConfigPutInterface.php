<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

use Interactiv4\Contracts\SPL\Config\Api\Exception\CouldNotPutConfigException;
use LogicException;
use RuntimeException;

/**
 * Interface ConfigPutInterface.
 *
 * Store internally / persist config.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface ConfigPutInterface
{
    /**
     * Store internally / persist config.
     * It MAY use optionally supplied context to determine how / where config should be stored / persisted.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param mixed $value   Mandatory, it can be of any type, recommended: int|string|bool
     * @param array $context Optional, additional data to determine how / where config should be stored / persisted.
     *
     * @return void Returning without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting config.
     *
     * @throws CouldNotPutConfigException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function setConfig(
        $value,
        array $context = []
    ): void;
}
