<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

use Interactiv4\Contracts\SPL\Config\Api\Exception\CouldNotReadConfigException;
use LogicException;
use RuntimeException;

/**
 * Interface ConfigReadInterface.
 *
 * Read current config.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface ConfigReadInterface
{
    /**
     * Read current config.
     * It MAY use optionally supplied context to determine how config should be retrieved.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional, additional data to determine how config should be retrieved.
     *
     * @return mixed Current config. It can be anything.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when reading config.
     *
     * @throws CouldNotReadConfigException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function getConfig(array $context = []);
}
