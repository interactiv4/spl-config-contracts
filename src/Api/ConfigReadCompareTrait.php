<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

use Interactiv4\Contracts\SPL\Config\Api\Exception\CouldNotCompareConfigException;
use Interactiv4\Contracts\SPL\Config\Api\Exception\CouldNotReadConfigException;
use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;

/**
 * Trait ConfigReadCompareTrait.
 *
 * For classes implementing both ConfigReadInterface and ConfigCompareInterface:
 * - Directly
 * - By using ConfigReadCompareInterface
 * - By using ConfigInterface
 *
 * isConfig() method can be implemented in a generic way. Php does not allow generic method bodies in interfaces yet,
 * so you can help yourself by using this trait to implement functionality in previously described cases.
 *
 * @see ConfigReadInterface
 * @see ConfigCompareInterface
 * @see ConfigReadCompareInterface
 * @see ConfigInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait ConfigReadCompareTrait
{
    use ExceptionWrapperTrait;

    /**
     * {@inheritdoc}
     */
    public function isConfig(
        $value,
        array $context = [],
        bool $strict = false
    ): bool {
        /** @var ConfigReadInterface|ConfigReadCompareInterface|ConfigInterface $this */
        try {
            $currentValue = $this->getConfig($context);
        } catch (CouldNotReadConfigException $e) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(CouldNotCompareConfigException::class, $e);
        }

        /** @noinspection TypeUnsafeComparisonInspection */
        return $strict
            ? $value === $currentValue
            : $value == $currentValue;
    }
}
