<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Config\Api;

/**
 * Interface ConfigReadCompareInterface.
 *
 * Read / compare current config.
 * Most of times, an implementation providing ConfigReadInterface capabilities, is also able to perform the adequate
 * comparison and provide ConfigCompareInterface capabilities by using the retrieved value.
 *
 * This is a convenience interface to avoid implementors having to implement separately both interfaces:
 *
 * @see ConfigReadInterface
 * @see ConfigCompareInterface
 * @see ConfigReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Config
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface ConfigReadCompareInterface extends
    ConfigReadInterface,
    ConfigCompareInterface
{
}
